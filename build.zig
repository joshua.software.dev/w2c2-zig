const std = @import("std");


fn build_w2c2(b: *std.Build, w2c2_dep: *std.Build.Dependency, args: anytype) *std.Build.Step.Compile {
    const exe = b.addExecutable(.{
        .name = "w2c2",
        // In this case the main source file is merely a path, however, in more
        // complicated build scripts, this could be a generated file.
        .target = args.target,
        .optimize = args.optimize,
    });

    const flags: []const []const u8 = &.{
        "-std=gnu90",
        "-Wall",
        "-Werror=conversion",
        "-Wextra",
        "-Wpedantic",
        "-Wunused-result",
        "-Wno-long-long",
        "-Wno-unused-function",
        "-fno-sanitize=undefined",
    };

    const files: []const []const u8 = &.{
        "w2c2/array.c",
        "w2c2/c.c",
        "w2c2/compat.c",
        "w2c2/debug.c",
        "w2c2/export.c",
        "w2c2/file.c",
        "w2c2/instruction.c",
        "w2c2/main.c",
        "w2c2/opcode.c",
        "w2c2/reader.c",
        "w2c2/section.c",
        "w2c2/sha1.c",
        "w2c2/stringbuilder.c",
        "w2c2/valuetype.c",
    };

    exe.defineCMacro("HAS_GETOPT", "1");
    exe.defineCMacro("HAS_LIBGEN", "1");
    exe.defineCMacro("HAS_STRDUP", "1");
    exe.defineCMacro("HAS_UNISTD", "1");

    switch (args.target.result.os.tag) {
        .linux, .macos => {
            exe.defineCMacro("HAS_GLOB", "1");
            exe.defineCMacro("HAS_PTHREAD", "1");
        },
        else => {},
    }

    exe.linkLibC();
    exe.addIncludePath(w2c2_dep.path("w2c2/"));
    exe.addCSourceFiles(.{
        .dependency = w2c2_dep,
        .files = files,
        .flags = flags,
    });

    return exe;
}

fn build_wasi(b: *std.Build, w2c2_dep: *std.Build.Dependency, args: anytype) *std.Build.Step.Compile {
    const lib = b.addStaticLibrary(.{
        .name = "w2c2wasi",
        // In this case the main source file is merely a path, however, in more
        // complicated build scripts, this could be a generated file.
        .target = args.target,
        .optimize = args.optimize,
    });

    const flags: []const []const u8 = &.{
        "-std=gnu90",
        "-Wall",
        "-Wextra",
        "-Wpedantic",
        "-Wunused-result",
        "-Wno-long-long",
        "-Wno-unused-function",
        "-fno-sanitize=undefined",
    };

    const files: []const []const u8 = &.{
        "wasi/mac.c",
        "wasi/wasi.c",
        "wasi/win32.c",
    };

    switch (args.target.result.os.tag) {
        .linux => {
            lib.defineCMacro("HAS_FCNTL", "1");
            lib.defineCMacro("HAS_GETENTROPY", "1");
            lib.defineCMacro("HAS_LSTAT", "1");
            lib.defineCMacro("HAS_STRNDUP", "1");
            lib.defineCMacro("HAS_SYSRESOURCE", "1");
            lib.defineCMacro("HAS_SYSTIME", "1");
            lib.defineCMacro("HAS_SYSUIO", "1");
            lib.defineCMacro("HAS_TIMESPEC", "1");
            lib.defineCMacro("HAS_UNISTD", "1");
            lib.defineCMacro("WASM_THREADS_PTHREADS", "1");
        },
        .macos => {
            lib.defineCMacro("HAS_FCNTL", "1");
            lib.defineCMacro("HAS_LSTAT", "1");
            lib.defineCMacro("HAS_STRNDUP", "1");
            lib.defineCMacro("HAS_SYSRESOURCE", "1");
            lib.defineCMacro("HAS_SYSTIME", "1");
            lib.defineCMacro("HAS_SYSUIO", "1");
            lib.defineCMacro("HAS_TIMESPEC", "1");
            lib.defineCMacro("HAS_UNISTD", "1");
            lib.defineCMacro("WASM_THREADS_PTHREADS", "1");
        },
        .windows => {
            lib.defineCMacro("HAS_SYSTIME", "1");
            lib.defineCMacro("HAS_TIMESPEC", "1");
            lib.defineCMacro("HAS_UNISTD", "1");
            lib.defineCMacro("WASM_THREADS_WIN32", "1");
        },
        else => {},
    }

    lib.linkLibC();
    lib.addIncludePath(w2c2_dep.path("wasi/"));
    lib.addCSourceFiles(.{
        .dependency = w2c2_dep,
        .files = files,
        .flags = flags,
    });

    return lib;
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const use_native_target = b.option(
        bool,
        "use_native_target",
        "When set to true, build the w2c2 executable for the host architecture, " ++
        "instead of the specified architecture. Default: true",
    ) orelse true;

    const w2c2_dep = b.dependency("w2c2", .{});

    const w2c2_exe = build_w2c2(b, w2c2_dep, .{
        .target = switch (use_native_target) {
            true => b.host,
            else => target,
        },
        .optimize = optimize,
    });
    const wasi_lib = build_wasi(b, w2c2_dep, .{ .target = target, .optimize = optimize });

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(w2c2_exe);
    b.installArtifact(wasi_lib);
}
